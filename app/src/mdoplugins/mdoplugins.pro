include(../defaults.pri)

TEMPLATE = subdirs
CONFIG+=ordered
SUBDIRS = \
    mdoplugin \
    coreplugin \
    hugoplugin \
    middlemanplugin
